## O:

Learned the concepts of cloud native and Had a review meeting.

## R:

I think the cloud native a bit unintelligible then the review session useful

## I:

- The first Retro meeting was held today to review what the group and individuals were doing well and what they were not doing well, and to make suggestions and plans for three areas of improvement. 
- By learning the concepts of containers, microservices and CI/CD during the process of speaking and listening to others, and through the teacher's supplemental explanation of Cloud Native, we gained a general knowledge of this piece of technology.



## D:

Through the retrospective, we can understand the different views and perspectives of each person in the group, and we really identified the problems today, and I will continue to improve according to the suggestions given in the meeting.