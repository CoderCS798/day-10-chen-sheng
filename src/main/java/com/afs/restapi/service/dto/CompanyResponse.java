package com.afs.restapi.service.dto;

public class CompanyResponse {
    private Long id;
    private String name;

    public CompanyResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CompanyResponse() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
